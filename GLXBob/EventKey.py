#!/usr/bin/env python
# -*- coding: utf-8 -*-

import curses
import sys


# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Tuuux <tuxa at rtnp dot org> all rights reserved


class EventKey(object):
    def __init__(self, screen=None):
        # Check it have a stdscr pass as argument
        if screen is None:
            self.screen = curses.initscr()
            # Turn off echoing of keys, and enter cbreak mode,
            # where no buffering is performed on keyboard input
            curses.noecho()
            curses.cbreak()

            # In keypad mode, escape sequences for special keys
            # (like the cursor keys) will be interpreted and
            # a special value like curses.KEY_LEFT will be returned
            self.screen.keypad(True)

        else:
            self.screen = screen
            # We consider the screen have been initialized on the right way

    def get_keyname(self, key_code):
        return curses.keyname(key_code)

    def get_ch(self):
        return self.screen.getch()

    def destroy(self):
        try:
            # Set everything back to normal
            self.screen.keypad(False)
            curses.echo()
            curses.nocbreak()
            curses.endwin()
            #self.screen = None
        finally:
            pass


if __name__ == '__main__':
    event_key = EventKey()
    try:
        event = event_key.get_ch()
        if event != -1:
            sys.stdout.write(u'{0}'.format(event))
            sys.stdout.flush()
            #event_key.destroy()

    except KeyboardInterrupt:
        event_key.destroy()
