#!/usr/bin/env python
# -*- coding: utf-8 -*-
import uuid
import logging


# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Tuuux <tuxa at rtnp dot org> all rights reserved


# http://www.pygtk.org/docs/pygobject/class-gobject.html
class EventBus(object):
    def __init__(self):
        self.signal_handlers = dict()
        self.blocked_handler = list()
        self.blocked_function = list()
        self.data = dict()

    def get_data(self, key):
        """
        The get_data() method returns the Python object associated with the specified key or
        None if there is no data associated with the key or if there is no key associated with the object.

        :param key: a string used as the key
        :type key: str
        :return: a Python object that is the value to be associated with the key
        """
        if key not in self._get_data_dict():
            return None
        elif not len(self._get_data_dict()[key]):
            return None
        else:
            return self._get_data_dict()[key]

    def set_data(self, key, data):
        """
        The set_data() method associates the specified Python object (data) with key.

        :param key: a string used as the key
        :param data: a Python object that is the value to be associated with the key
        :type key: str
        """
        self._get_data_dict()[key] = data

    def connect(self, detailed_signal, handler, *args):
        """
        The connect() method adds a function or method (handler)to the end of the list of signal handlers (yet a dict())
        for the named detailed_signal but before the default class signal handler.
        An optional set of parameters may be specified after the handler parameter.
        These will all be passed to the signal handler when invoked.

        :param detailed_signal: a string containing the signal name
        :type detailed_signal: str
        :param handler: function or method
        :type handler: object
        :arg *args: additional parameters arg1, arg2
        :return: handler_id
        :rtype: int
        """
        if detailed_signal not in self._get_signal_handlers_dict():
            self._get_signal_handlers_dict()[detailed_signal] = {}

        subscription = {
            'handler': handler,
            'argvs': args
        }
        handler_id = uuid.uuid1().int
        self._get_signal_handlers_dict()[detailed_signal][handler_id] = subscription
        logging.info('{0}: {1}'.format(
            self.__class__.__name__,
            self._get_signal_handlers_dict()[detailed_signal][handler_id]
        ))
        return int(handler_id)

    def disconnect(self, handler_id):
        """
        The disconnect() method removes the signal handler with the specified handler_id
        from the list of signal handlers for the object.

        :param handler_id: an int handler identifier
        :type handler_id: int
        :raise TypeError: if ``handler_id`` parameter is not a :py:data:`int` type
        """
        if type(handler_id) != int:
            raise TypeError(u'handler_id parameter must be a int type, like one returned by .connect() method')

        have_been_found = False
        for detailed_signal, info in self._get_signal_handlers_dict().items():
            for handler_id_found, _ in info.items():
                if handler_id_found == handler_id:
                    del self._get_signal_handlers_dict()[detailed_signal][handler_id]
                    have_been_found = detailed_signal
                    break
        if have_been_found:
            del self._get_signal_handlers_dict()[have_been_found]

    def handler_disconnect(self, handler_id):
        """
        The handler_disconnect() method removes the signal handler with the specified handler_id
        from the list of signal handlers for the object.

        :param handler_id: an int handler identifier
        :type handler_id: int
        :raise TypeError: if ``handler_id`` parameter is not a :py:data:`int` type
        """
        if type(handler_id) == int:
            self.disconnect(handler_id)
        else:
            raise TypeError(u'handler_id parameter must be a int type, like one returned by .connect() method')

    def handler_is_connected(self, handler_id):
        """
        The handler_is_connected() method returns True if the signal handler with the specified handler_id is
        connected to the object.

        :param handler_id: an int handler identifier
        :type handler_id: int
        :return: True if handler_id is found on signal handlers list
        :rtype: True or False
        :raise TypeError: if ``handler_id`` parameter is not a :py:data:`int` type
        """
        if type(handler_id) != int:
            raise TypeError(u'handler_id parameter must be a int type, like one returned by .connect() method')

        for detailed_signal, info in self._get_signal_handlers_dict().items():
            for handler_id_found, _ in info.items():
                if handler_id_found == handler_id:
                    return True
        return False

    def handler_block(self, handler_id):
        """
        The handler_block() method blocks the signal handler with the specified handler_id from being invoked until
        it is unblocked.

        :param handler_id: an int handler identifier
        :type handler_id: int
        :raise TypeError: if ``handler_id`` parameter is not a :py:data:`int` type
        """
        if type(handler_id) != int:
            raise TypeError(u'handler_id parameter must be a int type, like one returned by .connect() method')

        if handler_id not in self._get_blocked_handler():
            self._get_blocked_handler().append(handler_id)

    def handler_unblock(self, handler_id):
        """
        Remove a handler_id from the blocked list

        :param handler_id: an integer handler identifier
        :type handler_id: int
        :raise TypeError: if ``handler_id`` parameter is not a :py:data:`int` type
        """
        if type(handler_id) != int:
            raise TypeError(u'handler_id parameter must be a int type, like one returned by .connect() method')

        if handler_id in self._get_blocked_handler():
            self._get_blocked_handler().pop(self._get_blocked_handler().index(handler_id))

    def handler_block_by_func(self, callable_object):
        """
        The handler_block_by_func() method blocks the all signal handler connected to a specific callable from being
        invoked until the callable is unblocked.

        :param callable_object: the method or function it should be add on the blocked list
        :type callable_object: callable
        :raise TypeError: if ``callable_object`` parameter is not a :py:data:`callable` type
        """
        if callable(callable_object):
            if callable_object not in self._get_blocked_function():
                self._get_blocked_function().append(callable_object)
            else:
                pass
        else:
            raise TypeError(u'callable_object must be a callable object')

    def handler_unblock_by_func(self, callable_object):
        """
        The handler_unblock_by_func() method unblocks all signal handler connected to a specified callable there by
        allowing it to be invoked when the associated signals are emitted.

        :param callable_object: a callable python object
        :type callable_object: callable
        """
        if callable(callable_object):
            if callable_object not in self._get_blocked_function():
                pass
            else:
                self._get_blocked_function().pop(self._get_blocked_function().index(callable_object))
        else:
            raise TypeError(u'callable_object must be a callable object')

    def emit(self, detailed_signal, *args):
        """
        Emit a signal, for cann the associed callback via a name and give optional arguments to the callback.

        :param detailed_signal: a string containing the signal name
        :type detailed_signal: str
        :arg *args: additional parameters arg1, arg2
        """
        for subscription_name, info in self._get_signal_handlers_dict().items():
            # If the string contain on detailed_signal have been all ready registered
            if subscription_name == detailed_signal:
                # A subscription can have more of one callback identified by a handler_id
                for handler_id_found, _ in info.items():
                    # Check if it have permission to play the callback
                    if (handler_id_found not in self._get_blocked_handler()) and (
                        handler_id_found not in self._get_blocked_function()
                    ):
                        # Then log it
                        # print('{0}: EMIT {1}'.format(
                        #     self.__class__.__name__,
                        #     self._get_signal_handlers_dict()[subscription_name][handler_id_found]['handler']
                        # ))
                        logging.info('{0}: EMIT {1}'.format(
                            self.__class__.__name__,
                            self._get_signal_handlers_dict()[subscription_name][handler_id_found]
                        ))
                        # Finally execute the call back
                        return self._get_signal_handlers_dict()[subscription_name][handler_id_found]['handler'](*args)
                    else:
                        return None

    # Internal Function
    def _reset(self):
        # All subscribers will be cleared.
        self.signal_handlers = dict()
        self.blocked_handler = list()
        self.blocked_function = list()
        self.data = dict()

    def _get_signal_handlers_dict(self):
        return self.signal_handlers

    def _get_data_dict(self):
        return self.data

    def _get_blocked_handler(self):
        return self.blocked_handler

    def _get_blocked_function(self):
        return self.blocked_function
