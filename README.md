[![Documentation Status](https://readthedocs.org/projects/galaxie-bob/badge/?version=latest)](http://galaxie-bob.readthedocs.io/en/latest/?badge=latest)
[![build status](https://gitlab.com/Tuuux/galaxie-bob/badges/master/build.svg)](https://gitlab.com/Tuuux/galaxie-bob/commits/master)

Galaxie Bob
===========
- - - -
<p align="center">
<img src="https://gitlab.com/Tuuux/galaxie-curses/raw/master/docs/source/images/logo_galaxie.png">
</p>

Once upon a time, this project was hosted on a ancient platform called GitHub. Then came the Buyer.
The Buyer bought GitHub, willing to rule over its community.

I was not to sell, so here is the new home of "https://github.com/Tuuux/galaxie-bob".


The Project
-----------
The **galaxie-bob** is a python MainLoop object .

The MainLoop is a Alderson loop , it's mean not like infinity loop , that loop have MainLoop.run() and MainLoop.quit() method's for can stop or start the loop.

The MainLoop make it work and take a adaptive sleep time for impose a global Frame Rate.

That loop , should be a low power consumption, that was our target for the beginning.

Features
--------
* Alderson loop with run and quit method's
* Don't use 100% of CPU Time
* Frame Per Second with adaptive limitation
* Limitation will be apply with a knee (percentage) it depend of the Event list size

To Do
-----
* A Event Bus
* The Event list size should control interact with Frame Rate Limitation

Contribute
----------
- Issue Tracker: https://gitlab.com/Tuuux/galaxie-bob/issues
- Source Code: https://gitlab.com/Tuuux/galaxie-bob

Documentation
-------------
Readthedocs link: http://galaxie-bob.readthedocs.io/en/latest/index.html

License
-------
GNU GENERAL PUBLIC LICENSE Version 3
https://gitlab.com/Tuuux/galaxie-bob/blob/master/LICENSE