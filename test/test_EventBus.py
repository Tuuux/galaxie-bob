#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from random import randint
import sys
import os
import uuid

# Require when you haven't GLXBob as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXBob


# Unittest
class TestEventBus(unittest.TestCase):
    def setUp(self):
        # Before the test start
        self.event_bus = GLXBob.EventBus()

    @staticmethod
    def do_nothing(self):
        pass

    @staticmethod
    def emit_test1():
        return u'Success1'

    @staticmethod
    def emit_test2():
        return u'Success2'

    @staticmethod
    def emit_test3():
        return u'Success3'

    # Test Data
    def test_get_set_data(self):
        """EventBus: Test 'data' attribute with 'EventBus.set_data()' and 'EventBus.get_data()' method's """
        value_random_1 = str(randint(8, 250))
        value_random_2 = str(randint(8, 250))
        self.event_bus.set_data(value_random_1, value_random_2)
        self.assertEqual(self.event_bus.get_data(value_random_1), value_random_2)

    def test_get_data_return_false_if_key_not_exist(self):
        """EventBus: Test if 'EventBus.get_data()' method return False if key not exist"""
        self.assertFalse(self.event_bus.get_data(str(randint(8, 250))))

    def test_get_data_return_true_if_key_exist(self):
        """EventBus: Test if 'EventBus.get_data()' method return True if key exist"""
        value_random_1 = str(randint(8, 250))
        value_random_2 = str(randint(8, 250))
        self.event_bus.set_data(value_random_1, value_random_2)
        self.assertTrue(self.event_bus.get_data(value_random_1), value_random_2)

    #  Test Connect
    def test_if_connect_increase_signal_handlers_list_size(self):
        """EventBus: Test if signal_handlers_list increase when use EventBus.connect() """
        value_tested = len(self.event_bus.signal_handlers)
        value_random = str(randint(8, 250))
        self.event_bus.connect(
            detailed_signal=value_random,
            handler=self.do_nothing
        )
        self.assertEqual(value_tested + 1, len(self.event_bus.signal_handlers))

    def test_if_connect_not_increase_signal_handlers_list_size_if_signal_all_ready_exist(self):
        """EventBus: Test if signal_handlers_list not increase if signal all ready exist"""
        value_tested = len(self.event_bus.signal_handlers)
        value_random_1 = str(randint(8, 250))
        value_random_2 = str(randint(8, 250))
        # We test with one signal
        # add a new signal
        self.event_bus.connect(value_random_1, self.do_nothing)
        self.assertEqual(value_tested + 1, len(self.event_bus.signal_handlers))
        # then add the same signal many time (it should not increase the list)
        self.event_bus.connect(value_random_1, self.do_nothing)
        self.event_bus.connect(value_random_1, self.do_nothing)
        self.event_bus.connect(value_random_1, self.do_nothing)
        self.event_bus.connect(value_random_1, self.do_nothing)
        self.assertEqual(value_tested + 1, len(self.event_bus.signal_handlers))
        # We test with a other signal
        # for that we add a other signal (it should increase the list)
        self.event_bus.connect(value_random_2, self.do_nothing)
        self.assertEqual(value_tested + 2, len(self.event_bus.signal_handlers))
        # then add the same signal many time (it should not increase the list)
        self.event_bus.connect(value_random_2, self.do_nothing)
        self.event_bus.connect(value_random_2, self.do_nothing)
        self.event_bus.connect(value_random_2, self.do_nothing)
        self.event_bus.connect(value_random_2, self.do_nothing)
        self.assertEqual(value_tested + 2, len(self.event_bus.signal_handlers))

    def test_if_connect_return_a_long_type(self):
        """EventBus: Test if EventBus.connect() return a int handler_id"""
        value_random = str(randint(8, 250))
        handler_id = self.event_bus.connect(value_random, self.do_nothing)
        self.assertEqual(type(handler_id), int)

    def test_if_signal_is_store_in_the_list(self):
        """EventBus: Test if EventBus.connect() store something inside signal_handlers_list"""
        value_random_1 = str(randint(8, 250))
        handler_id = self.event_bus.connect(value_random_1, self.do_nothing)
        value_tested = self.do_nothing
        self.assertEqual(value_tested, self.event_bus.signal_handlers[value_random_1][handler_id]['handler'])

    # EventBus.disconnect()
    def test_if_disconnect_remove_signal(self):
        """EventBus: Test if EventBus.disconnect() remove a connected signal inside signal_handlers_list"""
        value_original = self.event_bus.signal_handlers
        value_list_size = len(self.event_bus.signal_handlers)
        value_detailed_name = str(randint(8, 250))
        value_handler = self.do_nothing
        value_handler_id = self.event_bus.connect(value_detailed_name, value_handler)
        # Check if we have store the key
        self.assertEqual(
            value_handler, self.event_bus.signal_handlers[value_detailed_name][value_handler_id]['handler']
        )
        # check if the size have been increase by 1
        self.assertEqual(value_list_size + 1, len(self.event_bus.signal_handlers))
        # remove the handler_id
        self.event_bus.disconnect(handler_id=value_handler_id)
        # check if the size have decrease by 1 (back to the original size)
        self.assertEqual(value_list_size, len(self.event_bus.signal_handlers))
        # check if the list id back to original
        self.assertEqual(value_original, self.event_bus.signal_handlers)

    def test_disconnect_raise_typeerror(self):
        """EventBus: Test raise TypeError when disconnect() with worng type"""
        self.assertRaises(TypeError, self.event_bus.disconnect, str("Hello"))

    # EventBus.handler_is_connected()
    def test_if_handler_is_connected_return_true_if_handler_id_is_found(self):
        """EventBus: Test if EventBus.handler_is_connected() return True if handler_id is found"""
        # We have a handler to nothing
        value_handler = self.do_nothing
        # Generate a randomize name
        value_detailed_name = str(randint(8, 250))
        # We connect a true handler and test if handler_is_connected() return the good value
        value_handler_id = self.event_bus.connect(value_detailed_name, value_handler)
        self.assertTrue(self.event_bus.handler_is_connected(value_handler_id))

    def test_handler_is_connected_raise_typeerror(self):
        """EventBus: Test raise TypeError when handler_is_connected() with worng type"""
        self.assertRaises(TypeError, self.event_bus.handler_is_connected, str("Hello"))

    def test_if_handler_is_connected_return_false_if_handler_id_is_not_found(self):
        """EventBus: Test if EventBus.handler_is_connected() return False if handler_id is not found"""
        # We test if a stupid handler id can be found,(it should return False)
        self.assertFalse(self.event_bus.handler_is_connected(uuid.uuid1().int))

    # EventBus.handler_block()
    def test_if_handler_block_add_the_handler_to_the_blocked_handler(self):
        """EventBus: Test if EventBus.handler_block() add the handler_id to blocked_handler list"""
        uuid_tested = uuid.uuid1().int
        self.event_bus.handler_block(uuid_tested)
        self.assertListEqual(self.event_bus.blocked_handler, [uuid_tested])

    def test_blocked_handler_raise_typeerror(self):
        """EventBus: Test raise TypeError when blocked_handler() with worng type"""
        self.assertRaises(TypeError, self.event_bus.blocked_handler, randint(1, 250))

    # EventBus.handler_unblock()
    def test_if_handler_unblock_remove_the_handler_from_the_blocked_handler(self):
        """EventBus: Test if EventBus.handler_unblock() remove the handler_id from blocked_handler list"""
        uuid_tested = uuid.uuid1().int
        self.event_bus.handler_block(uuid_tested)
        # check if it's add
        self.assertListEqual(self.event_bus.blocked_handler, [uuid_tested])
        # unblock and check again
        self.event_bus.handler_unblock(uuid_tested)
        self.assertListEqual(self.event_bus.blocked_handler, list())
        # Redo the same action for test if it work when the searching value not exist
        self.event_bus.handler_unblock(uuid_tested)
        self.assertListEqual(self.event_bus.blocked_handler, list())

    def test_handler_unblock_raise_typeerror(self):
        """EventBus: Test raise TypeError when handler_unblock() with worng type"""
        self.assertRaises(TypeError, self.event_bus.handler_unblock, str("Hello"))

    # EventBus.handler_block_by_func()
    def test_if_handler_block_by_func_add_a_function_inside_blocked_function_list(self):
        """EventBus: Test if EventBus.handler_block_by_func() add a function to blocked_function list"""
        # We have a handler to nothing
        value_handler = self.do_nothing
        # add the function inside the blocking list
        self.event_bus.handler_block_by_func(value_handler)
        # check if it's add
        self.assertListEqual(self.event_bus.blocked_function, [value_handler])

    def test_handler_block_by_func_raise_typeerror(self):
        """EventBus: Test raise TypeError when handler_block_by_func() with worng type"""
        self.assertRaises(TypeError, self.event_bus.handler_block_by_func, randint(1, 250))

    # EventBus.handler_unblock_by_func()
    def test_if_handler_unblock_by_func_remove_a_function_from_blocked_function_list(self):
        """EventBus: Test if EventBus.handler_unblock_by_func() remove a function from blocked_function list"""
        # We have a handler to nothing
        value_handler = self.do_nothing
        # add the function inside the blocking list
        self.event_bus.handler_block_by_func(value_handler)
        # check if it's ok
        self.assertListEqual(self.event_bus.blocked_function, [value_handler])
        # unblock and check again
        self.event_bus.handler_unblock_by_func(value_handler)
        self.assertListEqual(self.event_bus.blocked_function, list())

    def test_handler_unblock_by_func_raise_typeerror(self):
        """EventBus: Test raise TypeError when handler_unblock_by_func() with worng type"""
        self.assertRaises(TypeError, self.event_bus.handler_unblock_by_func, randint(1, 250))

    def test_emit(self):
        """EventBus: Test if EventBus.emit() work as it should"""
        self.event_bus.connect('test1', self.emit_test1)
        value_returned = self.event_bus.emit('test1')
        self.assertEqual(value_returned, u'Success1')

        self.event_bus.connect('test2', self.emit_test2)
        value_returned = self.event_bus.emit('test2')
        self.assertEqual(value_returned, u'Success2')

        self.event_bus.connect('test3', self.emit_test3)
        value_returned = self.event_bus.emit('test3')
        self.assertEqual(value_returned, u'Success3')

