.. title:: Galaxie BoB's documentation!

=======================================
Welcome to Galaxie BoB's documentation!
=======================================

Once upon a time, this project was hosted on a
ancient platform called GitHub. Then came the Buyer.
The Buyer bought GitHub, willing to rule over its community.
I was not to sell, so here is the new home of "https://github.com/Tuuux/galaxie-bob".

The Project
===========
The **galaxie-bob** is a python MainLoop object .

The MainLoop is a Alderson loop , it's mean not like infinity loop , that loop have MainLoop.run() and MainLoop.quit() method's for can stop or start the loop.

The MainLoop make it work and take a adaptive sleep time for impose a global Frame Rate.

That loop , should be a low power consumption, that was our target for the beginning.

Features
========
* Alderson loop with run and quit method's
* Don't use 100% of CPU Time
* Frame Per Second with adaptive limitation
* Limitation will be apply with a knee (percentage) it depend of the Event list size

To Do
=====
* A Event Bus
* The Event list size should control interact with Frame Rate Limitation

Contribute
==========
- Issue Tracker: https://gitlab.com/Tuuux/galaxie-bob/issues
- Source Code: https://gitlab.com/Tuuux/galaxie-bob

.. toctree::
   :maxdepth: 2

   GLXBob
   Basic-Types


License
=======
GNU GENERAL PUBLIC LICENSE Version 3


See the LICENCE_

.. _LICENCE: https://gitlab.com/Tuuux/galaxie-bob/blob/master/LICENSE

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
