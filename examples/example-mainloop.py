#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Tuuux <tuxa at rtnp dot org> all rights reserved

import os
import sys
import logging

# Require when you haven't GLXBob as default Package
current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(current_dir))
import GLXBob


def stop():
    mainloop.quit()
    #print(u'\rcoucou')


if __name__ == '__main__':
    logging.basicConfig(filename='/tmp/galaxie-bob.log',
                        level=logging.DEBUG,
                        format='%(asctime)s, %(levelname)s, %(message)s'
                        )
    logging.info('Started glxbob-demo')

    # Create the mainloop
    mainloop = GLXBob.MainLoop()

    # 60 FPS is not so bad ...
    mainloop.get_timer().set_fps_max(6.0)

    # Register a Event
    mainloop.get_event_bus().connect('QUIT', stop)


    # The Start
    mainloop.run()

    # The End
    sys.exit()
